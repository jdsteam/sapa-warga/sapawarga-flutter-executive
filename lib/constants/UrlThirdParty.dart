class UrlThirdParty {
  static const String apiHumasJabar = 'http://humas.jabarprov.go.id/api';
  static const String newsHumasJabarTerkini =
      'http://humas.jabarprov.go.id/terkini';
  static const String newsHumasJabar = apiHumasJabar + '/berita-terkini';
  static const String privacyPolicy =
      'https://digitalservice.jabarprov.go.id/index.php/privacy-policy-2/';
  static const String termOfService =
      'https://digitalservice.jabarprov.go.id/index.php/term-of-service/';
  static String urlCommunityGuideLine = 'https://digitalservice.jabarprov.go.id/index.php/panduan-komunitas-sapawarga/';

  static String urlPathTwitter ='http://mobile.twitter.com/';
  static String urlPathInstagram ='https://www.instagram.com/';
  static String urlPathFacebook ='fb://facewebmodal/f?href=';

}
