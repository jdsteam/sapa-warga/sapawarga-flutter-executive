class Dictionary {
  static String appName = 'Sapawarga';
  static String version = '0.0.3';
  static String loadTwitterUrlFailed = 'Link akun twitter anda belum dipasang';

  // ==============Error messages =========
  static String errorConnection = 'Periksa kembali koneksi Internet';
  static String errorIOConnection = 'Koneksi ke server bermasalah';
  static String errorUrl = 'Tidak bisa mengakses halaman';
  static String errorLogin = 'Terjadi kesalahan saat login';
  static String errorTimeOut = 'Server tidak merespon';
  static String errorNotFound = 'Data tidak ditemukan';
  static String errorUnauthorized = 'Izin akses ke server ditolak';
  static String errorInternal = 'Server sedang dalam perbaikan';
  static String errorExternal =
      'Terjadi kesalahan periksa kembali koneksi internet anda';
  static String errorGetEmail = 'Gagal mendapatkan email, coba lagi';
  static String somethingWrong = 'Terjadi kesalahan';

  // ==============Error messages =========

  // =========form validations==============

  // Validation login
  static String errorEmptyUsername = 'Username harus diisi';
  static String errorMinimumUsername = 'Username minimal 4 karakter';
  static String errorMaximumUsername =
      'Username melebihi batas maksimal karakter';
  static String errorInvalidUsername =
      'huruf kecil, angka, underscore dan titik diperbolehkan';
  static String errorEmptyPassword = 'Kata Sandi harus diisi';
  static String errorMinimumPassword = 'Kata Sandi minimal 6 karakter';
  static String errorMaximumPassword = 'Kata Sandi maksimal 255 karakter';
  static String termOfService = 'Term of Service';
  static String privacyPolicy = 'Privacy Policy';

  // =========form validations===`===========

  // login screen
  static String loginLoading = 'Masuk...';
  static String loginButton = 'Masuk';
  static String copyRight = 'Hak Cipta Pemerintah Provinsi Jawa Barat';

  // Form Label & hint
  static String labelUsername = 'Username';
  static String labelPassword = 'Kata Sandi';
  static String labelPasswordNew = 'Masukkan Kata Sandi Baru';
  static String labelPasswordNewConfirm = 'Ulangi Kata Sandi Baru';
  static String labelPasswordOld = 'Masukkan Kata Sandi Lama';

  static String loadingData = 'Sedang mengambil data ...';

  static String hintComment = 'Tulis komentar ...';
  static String hintPasswordNew = 'Kata Sandi Baru';
  static String hintPasswordOld = 'Kata Sandi Lama';



  // Form hint

  //Menu & Title
  static String activities = 'Kegiatan';
  static String successAddComment = 'Komentar berhasil diposting';
  static String heroImageTag = 'preview';
  static String address = 'Alamat';

  static String save = 'Simpan';

  //Dialog Message
  static String permissionCamera =
      'Untuk mengambil foto, izinkan aplikasi Sapawarga untuk mengakses kamera Anda.';
  static String permissionGalery =
      'Untuk membuka gallery, izinkan aplikasi Sapawarga untuk mengakses penyimpanan Anda.';
  static String permissionRWActivities =
      'Untuk memposting kegiatan RW, izinkan aplikasi Sapawarga untuk mengakses penyimpanan dan kamera Anda.';
  static String confirmExitTitle = 'Tutup Aplikasi?';
  static String confirmExit = 'Yakin akan keluar dari Sapawarga?';
  static String permissionLocationMap =
      'Untuk menampilkan map, izinkan aplikasi Sapawarga untuk mengakses lokasi Anda.';
  static String changePassword = 'Ubah Kata Sandi';

  //Menu others
  static String yes = 'Ya';
  static String no = 'Tidak';
  static String cancel = 'Batal';
  static String congratulation = 'Selamat';
  static String setYourLocation = 'Set lokasi tempat tinggal anda';
  static String setLocation = 'Set Lokasi...';
  static String useGoogleAccount = 'Gunakan akun Google';
  static String useYourEmail = 'Gunakan alamat email anda';
  static String successChangePassword = 'Kata sandi berhasil diubah';
  static String forgotPassword = 'Lupa kata sandi?';


  //RW Activities
  static String emptyDataRWActivity =
      'Tidak ada data dalam list Kegiatan RW';
  static String emptyComments = 'Belum ada komentar';
  static String contentNotPermittedRWActivities = 'Postingan anda tidak sesuai dengan panduan komunitas.';
  static String readMore = ' Baca Selengkapnya';
  static String postRWActivitiesRejected= 'Post kegiatan ditolak';
  static String takePhoto = 'Ambil foto';
  static String takePhotoFromGallery = 'Ambil dari gallery';
  static String later = 'Nanti';
  static String next = 'Lanjut';
  static String hintSearch = 'Ketikkan kata kunci ...';

  //From Edit Profile
  static String contact = 'Kontak';
  static String profile = 'Profil';
  static String updatePhotoTitle = 'Update Foto.';
  static String ok = 'Ok';
  static String loadInstagramUrlFailed =
      'Link akun instagram anda belum dipasang';
  static String loadFacebookUrlFailed =
      'Link akun facebook anda belum dipasang';
  static String name = 'Nama';
  static String birthDate = 'Tanggal Lahir';
  static String education = 'Pendidikan';
  static String job = 'Pekerjaan';
  static String username = 'Username';
  static String email = 'Email';
  static String telephone = 'Telepon';
  static String noTelp = 'Nomor Telepon';
  static String fullAddress = 'Alamat Lengkap';
  static String kabkota = 'Kabupaten / Kota';
  static String kecamatan = 'Kecamatan';
  static String kelurahan = 'Desa/Kelurahan';
  static String rw = 'Rw';
  static String rt = 'Rt';
  static String role = 'Role';
  static String socmed = 'Sosial Media';
  static String instagram = 'Instagram';
  static String facebook = 'Facebook';
  static String twitter = 'Twitter';
  static String saveProfile = 'Simpan';

  // place holder edit profile
  static String placeHolderName = 'Masukan nama';
  static String placeHolderBirthday = 'Masukan tanggal lahir';
  static String placeHolderEducation = 'Masukan pendidikan';
  static String placeHolderJob = 'Masukan Pekerjaan';
  static String placeHolderUsername = 'Masukan username';
  static String placeHolderEmail = 'Masukan email';
  static String placeHolderNoTelp = 'Masukan no telepon';
  static String placeHolderAddress = 'Masukan alamat';
  static String placeHolderRt = 'Masukan RT';
  static String placeHolderTwitter = 'Masukan twitter';
  static String placeHolderInstagram = 'Masukan instagram';
  static String placeHolderFacebook = 'Masukan facebook';

  static String successSaveProfile = 'Perubahan data berhasil disimpan.';
  static String successSavePhoto = 'Foto Profile telah di rubah';

  // Form Validation Error
  static String errorEmptyEmail = 'Email harus diisi';
  static String errorInvalidEmail = 'Email tidak valid';
  static String errorEmptyConfirmPassword = 'Konfirmasi kata sandi harus diisi';
  static String errorNotMatchPassword = 'Konfirmasi kata sandi tidak sesuai';
  static String errorEmptyName = 'Nama harus diisi';
  static String errorMinimumName =
      'Nama harus terdiri dari 4 karakter atau lebih';
  static String errorMaximumName = 'Nama harus kurang dari 255 karakter';
  static String errorInvalidName = 'Format nama tidak sesuai';
  static String errorMinimumPhone =
      'Nomor telepon harus terdiri 3 sampai 13 nomor';
  static String errorMaximumPhone = 'Nomor Telepon harus kurang dari 13 nomor';
  static String errorEmptyPhone = 'Nomor Telepon harus diisi';
  static String errorInvalidPhone = 'Format nomor telepon tidak sesuai';
  static String errorUserNotFound = 'Tidak ada user dengan email tersebut';
  static String errorMaximumAddress = 'Alamat harus kurang dari 255 karakter';
  static String errorEmptyAddress = 'Alamat harus diisi';
  static String errorMaximumRT = 'Data input tidak sesuai';
  static String errorEmptyRT = 'RT harus diisi';
  static String errorInvalidRT = 'Format RT tidak sesuai';
  static String errorInvalidInstagram = 'Format instagram tidak sesuai';
  static String errorInvalidTwitter = 'Format twitter tidak sesuai';
  static String errorInvalidFacebook = 'Format facebook tidak sesuai';
  static String errorMinimumQuestion = 'Pertanyaan minimal 10 karakter';
}
