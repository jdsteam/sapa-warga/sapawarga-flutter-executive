import 'package:flutter/material.dart';

class NavigationConstrants {
  static final navKey = GlobalKey<NavigatorState>();

  // Pages routes
  static const String login = '/login';
  static const String home = '/home';
  static const String SubProfile = "/sub-profile";
  static const String SubContact = "/sub-contact";
  static const String SubAddress = "/sub-address";
  static const String Browser = "/browser";
  static const String ChangePassword = "/change-password";
}
