import 'package:flutter/material.dart';
import 'package:sapawarga_executive/constants/BrowserScreen.dart';
import 'package:sapawarga_executive/constants/Navigation.dart';
import 'package:sapawarga_executive/screens/account/submenuprofile/SubAdressScreen.dart';
import 'package:sapawarga_executive/screens/account/submenuprofile/SubContactScreen.dart';
import 'package:sapawarga_executive/screens/account/submenuprofile/SubProfileScreen.dart';
import 'package:sapawarga_executive/screens/changePassword/ChangePasswordScreen.dart';
import 'package:sapawarga_executive/screens/home/homeScreen.dart';
import 'package:sapawarga_executive/screens/login/loginScreen.dart';

Route generateRoutes(RouteSettings settings) {
  // getting arguments passed
  final args = settings.arguments;

  switch (settings.name) {
    case NavigationConstrants.login:
      return buildRoute(
          settings,
          LoginScreen(
            userRepository: args,
          ));
    case NavigationConstrants.home:
      return buildRoute(settings, HomeScreen());
    case NavigationConstrants.SubProfile:
      return buildRoute(settings, SubProfileScreen());
    case NavigationConstrants.SubContact:
      return buildRoute(settings, SubContactScreen());
    case NavigationConstrants.SubAddress:
      return buildRoute(settings, SubAdressScreen());
    case NavigationConstrants.ChangePassword:
      return buildRoute(settings, ChangePasswordScreen(type: args));
    case NavigationConstrants.Browser:
      return buildRoute(
          settings,
          BrowserScreen(
            url: args,
          ));

    default:
      return null;
  }
}

MaterialPageRoute buildRoute(RouteSettings settings, Widget builder) {
  return MaterialPageRoute(
    settings: settings,
    builder: (BuildContext context) => builder,
  );
}
