import 'dart:convert';

import 'package:sapawarga_executive/constants/preferencesName.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPreferences {
  // ============== TOKEN ======================
  static Future<void> setToken(String token) async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString(PreferencesName.token, token);
    return;
  }

  static Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString(PreferencesName.token);
  }

  static Future<String> deleteToken() async {

    await Future.delayed(Duration(seconds: 1));

    final prefs = await SharedPreferences.getInstance();

    await prefs.remove(PreferencesName.token);

    return 'delete success';

  }

  static Future<bool> hasToken() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString(PreferencesName.token) == null ? false : true;
  }
  // ============== TOKEN ======================

  // ============== USER INFO ======================

  static Future<void> setUserInfo(Data authUserInfo) async {
    final prefs = await SharedPreferences.getInstance();

    var authUserInfoJson = userDataInfoToJson(authUserInfo);

    await prefs.setString(
        PreferencesName.userInfo, json.encode(authUserInfoJson));
  }

  static Future<Data> getLocalUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    String userInfoJsonString = prefs.getString(PreferencesName.userInfo);
    UserInfoModel userInfoMap = userInfoModelFromJson(jsonDecode(userInfoJsonString));
    return userInfoMap.data;
  }

  static Future<bool> hasLocalUserInfo() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString(PreferencesName.userInfo) == null ? false : true;
  }
  // ============== USER INFO ======================
}
