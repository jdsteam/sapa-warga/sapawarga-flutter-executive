import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga_executive/components/DialogRequestPermission.dart';
import 'package:sapawarga_executive/constants/Dictionary.dart';

class ImagePickerHelper {

  final BuildContext context;

  ImagePickerHelper(this.context);

  Future<File> openDialog() async {

    File _image;

    await showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.camera),
                  title: Text(Dictionary.takePhoto),
                  onTap: () async {
                    _image = await _permissionCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.image),
                  title: Text(Dictionary.takePhotoFromGallery),
                  onTap: () async {
                    _image = await _permissionGallery();
                  },
                ),
              ],
            ),
          );
        });

    return _image;
  }

  Future<File> _permissionCamera() async {
    PermissionStatus permission =
    await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permission != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
            image: Image.asset(
              'assets/icons/photo-camera.png',
              fit: BoxFit.contain,
              color: Colors.white,
            ),
            description: Dictionary.permissionCamera,
            onOkPressed: () {
              Navigator.of(context).pop();
              PermissionHandler().requestPermissions(
                  [PermissionGroup.camera]).then(_onStatusRequestedCamera);
            },
          ));
      return null;
    } else {
      return await _openCamera();
    }
  }

  Future<File> _permissionGallery() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
            image: Image.asset(
              'assets/icons/folder.png',
              fit: BoxFit.contain,
              color: Colors.white,
            ),
            description: Dictionary.permissionGalery,
            onOkPressed: () {
              Navigator.of(context).pop();
              PermissionHandler().requestPermissions(
                  [PermissionGroup.storage]).then(_onStatusRequestedStorage);
            },
          ));
      return null;
    } else {
      return await _openGallery();
    }
  }

  void _onStatusRequestedStorage(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _permissionGallery();
    } else {
      Navigator.of(context).pop();
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permissionCamera();
    } else {
      Navigator.of(context).pop();
    }
  }

  Future<File> _openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 1080, maxWidth: 1080);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        Navigator.of(context).pop();
          return image;
      } else {
        Navigator.of(context).pop();
        return null;
      }
    } else {
      Navigator.of(context).pop();
      return null;
    }
  }

  Future<File> _openGallery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 1080, maxWidth: 1080);

    if (image != null && image.path != null) {
      image = await FlutterExifRotation.rotateImage(path: image.path);
      if (image != null) {
        Navigator.of(context).pop();
        return image;
      } else {
        Navigator.of(context).pop();
        return null;
      }
    } else {
      Navigator.of(context).pop();
      return null;
    }
  }
}