enum AttachmentType {
  phonebook_photo,
  aspirasi_photo,
  news_photo,
  user_post_photo,
  banner_photo,
  popup_photo,
  user_photo,
  news_important,
  news_important_attachment
}
