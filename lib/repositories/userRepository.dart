import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/models/LoginModel.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart' as prefix0;
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/ValidationException.dart';
import 'package:sapawarga_executive/constants/EndPointPath.dart';
import 'package:sapawarga_executive/constants/ErrorException.dart';
import 'package:sapawarga_executive/constants/HttpHeaders.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga_executive/utilities/sharedPreferences/authPreferences.dart';

class UserRepository {
  Future<String> authenticate({
    @required String username,
    @required String password,
    String fcmToken,
  }) async {
    await Future.delayed(Duration(seconds: 1));

    Map requestData = {
      'LoginForm': {
        'username': username,
        'password': password,
//        'push_token': fcmToken
      }
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post('${EndPointPath.login}',
            headers: await HttpHeaders.headers(), body: requestBody)
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {

      var responseData = LoginModel.fromJson(json.decode(response.body)) ;

      return responseData.data.accessToken;
    }

    if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);

      throw ValidationException(responseData['data']);
    }

    throw Exception(ErrorException.loginException);
  }

  Future<prefix0.Data> fetchUserInfo() async {
    await Future.delayed(Duration(seconds: 1));

    String token = await AuthPreferences.getToken();

    var response = await http.get(
      '${EndPointPath.profile}',
      headers: await HttpHeaders.headers(token: token),
    );
  var dataUser = UserInfoModel.fromJson(json.decode(response.body));

    return dataUser.data;
  }

  Future<prefix0.Data> getUserInfo({bool forceFetch = false}) async {
    prefix0.Data authUserInfo;
    bool hasUserInfo = await AuthPreferences.hasLocalUserInfo();
    if (hasUserInfo == false || forceFetch == true) {
      authUserInfo = await fetchUserInfo();
      await AuthPreferences.setUserInfo(authUserInfo);
    } else {
      authUserInfo = await AuthPreferences.getLocalUserInfo();
    }

    return authUserInfo;
  }

  Future<void> unAuthenticate() async {
    await Future.delayed(Duration(seconds: 1));

    String token =  await AuthPreferences.getToken();

    try {
      var response =  await http.post(
        '${EndPointPath.logout}',
        headers: await HttpHeaders.headers(token: token),
      ).timeout(Duration(seconds: 10));

      print(response.body.toString());
    } catch (e) {
      print(e.toString());
    }

    await AuthPreferences.deleteToken();
    await AuthProfileRepository().deleteLocalUserInfo();
  }

}
