import 'dart:convert';
import 'dart:io';

import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/EndPointPath.dart';
import 'package:sapawarga_executive/constants/ErrorException.dart';
import 'package:sapawarga_executive/constants/HttpHeaders.dart' as prefix0;
import 'package:sapawarga_executive/enum/AttachmentType.dart';
import 'package:sapawarga_executive/models/AddPhotoModel.dart';
import 'package:sapawarga_executive/models/RWActivityCommentModel.dart';
import 'package:sapawarga_executive/models/RWActivityModel.dart';
import 'package:sapawarga_executive/repositories/AttachmentRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/ValidationException.dart';
import 'package:sapawarga_executive/utilities/sharedPreferences/authPreferences.dart';

class RWActivityRepository {

  //get data general RW Activities
  Future<RWActivityModel> fetchRecords({int page}) async {
    String token = await AuthPreferences.getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwActivities}?page=$page&limit=20',
            headers: await prefix0.HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data my activities
  Future<RWActivityModel> fetchRecordsMyActivities({int page}) async {
    String token = await AuthPreferences.getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${EndPointPath.rwMyActivities}?page=$page&limit=20',
        headers: await prefix0.HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //search data general RW Activities
  Future<RWActivityModel> searchRecord({@required String keyword, @required int page, bool isMyActivity = false}) async {
    String token = await AuthPreferences.getToken();

    await Future.delayed(Duration(milliseconds: 500));

    final response = await http
        .get('${isMyActivity ? EndPointPath.rwMyActivities : EndPointPath.rwActivities}?search=$keyword&page=$page&limit=20',
        headers: await prefix0.HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for send status like or not on activities
  Future<bool> sendLike({@required int id}) async {
    String token = await AuthPreferences.getToken();

    var response = await http
        .post('${EndPointPath.rwActivities}/likes/$id',
            headers: await prefix0.HttpHeaders.headers(token: token))
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.statusCode);
    }
  }

  //send new Rw activities
  Future<ItemRWActivity> postActivity({@required File image, @required String description}) async {
    String token = await AuthPreferences.getToken();

    AddPhotoModel responseImage = await AttachmentRepository()
        .addPhoto(image, AttachmentType.user_post_photo);

    Map requestData = {
      'text': description,
      'image_path': responseImage.data.path,
      'status': 10,
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post('${EndPointPath.rwActivities}',
            headers: await prefix0.HttpHeaders.headers(token: token), body: requestBody)
        .timeout(Duration(seconds: 10));

    if (response.statusCode == 201) {
      var data = jsonDecode(response.body)['data'];

      ItemRWActivity resData = itemRWActivityFromJson(jsonEncode(data));

      return resData;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data comment from Activities
  Future<RWActivityCommentModel> fetchCommentRecords({@required int id, int page}) async {
    String token = await AuthPreferences.getToken();

    final response = await http
        .get('${EndPointPath.rwActivities}/$id/comments?page=$page&limit=20',
            headers: await prefix0.HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = rwActivityCommentModelFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for send comment on activities
  Future<ItemRwActivityComment> postComment({@required int id, @required String text}) async {
    String token =  await AuthPreferences.getToken();

    Map requestData = {
      'user_post_id': id,
      'text': text,
      'status': 10,
    };

    var requestBody = json.encode(requestData);

    final response = await http
        .post('${EndPointPath.rwActivities}/$id/comments',
            headers: await prefix0.HttpHeaders.headers(token: token), body: requestBody)
        .timeout(const Duration(seconds: 10));

    print(response.body);

    if (response.statusCode == 201) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = itemRWActivityCommentFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
