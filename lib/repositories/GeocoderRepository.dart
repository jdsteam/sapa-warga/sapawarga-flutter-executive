
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/ErrorException.dart';


class GeocoderRepository {
  Future<Address> getAddress(LatLng coordinate) async {

    var response = await http.get('https://www.google.com').timeout(Duration(seconds: 10));

    if (response.statusCode == 200) {
      final coordinates = Coordinates(
          coordinate.latitude, coordinate.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      return addresses.first;
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}