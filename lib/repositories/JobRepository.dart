import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/EndPointPath.dart';
import 'package:sapawarga_executive/constants/ErrorException.dart';
import 'package:sapawarga_executive/constants/HttpHeaders.dart';
import 'package:sapawarga_executive/models/GeneralTitleModel.dart';
import 'package:sapawarga_executive/utilities/sharedPreferences/authPreferences.dart';

class JobRepository {
  Future<List<GeneralTitleModel>> fetchJobs() async {
    await Future.delayed(Duration(seconds: 1));

    String token =  await AuthPreferences.getToken();

    final response = await http
        .get(EndPointPath.jobs,
            headers: await HttpHeaders.headers(token: token))
        .timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listGeneralTitleModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
