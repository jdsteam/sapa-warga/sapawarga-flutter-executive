import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PasswordFormField extends StatefulWidget {
  final TextEditingController controller;
  final bool showIcon;
  final bool hasFloatingPlaceholder;
  final String labelText;
  final String hintText;
  final String errorText;
  final String Function(String) validator;

  PasswordFormField(
      {Key key,
      this.labelText,
      this.showIcon = true,
      this.hasFloatingPlaceholder,
      this.hintText,
      this.errorText,
      this.controller,
      this.validator})
      : super(key: key);

  _PasswordFormFieldState createState() => _PasswordFormFieldState();
}

class _PasswordFormFieldState extends State<PasswordFormField> {
  TextEditingController get _controller => widget.controller;

  String get _labelText => widget.labelText;

  String get _hintText => widget.hintText;

  String get _errorText => widget.errorText;

  String Function(String) get _validator => widget.validator;

  bool get _showIcon => widget.showIcon;

  bool get _hasFloatingPlaceholder => widget.hasFloatingPlaceholder;

  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        TextFormField(
            controller: _controller,
            obscureText: _obscureText,
            decoration: InputDecoration(
                hasFloatingPlaceholder: _hasFloatingPlaceholder,
                hintText: _hintText,
                icon: _showIcon ? Icon(Icons.lock) : null,
                contentPadding: EdgeInsets.only(
                    left: 12.0, right: 45.0, top: 14.0, bottom: 14.0),
                labelText: _labelText,
                errorText: _errorText,
                border: OutlineInputBorder()),
            validator: _validator),
        Positioned(
          top: 12.0,
          right: 12.0,
          child: GestureDetector(
            child: SizedBox(
              height: 24.0,
              child: Icon(
                _obscureText ? Icons.visibility : Icons.visibility_off,
                color: Colors.grey[700],
              ),
            ),
            onTap: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            },
          ),
        )
      ],
    );
  }
}
