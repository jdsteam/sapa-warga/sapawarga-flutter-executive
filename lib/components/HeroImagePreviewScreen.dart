import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';

class HeroImagePreview extends StatefulWidget {
  final String heroTag;
  final String imageUrl;

  HeroImagePreview(this.heroTag, this.imageUrl);

  @override
  _HeroImagePreviewState createState() => _HeroImagePreviewState();
}

class _HeroImagePreviewState extends State<HeroImagePreview> {

  @override
  initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
              child: PhotoView(
                imageProvider: NetworkImage(widget.imageUrl),
                minScale: 0.3,
                backgroundDecoration: BoxDecoration(
                  color: Colors.white
                ),
                heroAttributes: PhotoViewHeroAttributes(tag: widget.heroTag),
                onTapUp: (context, tapDetail, controller) {
                  Navigator.of(context).pop();
                },
              ),
            );
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }
}
