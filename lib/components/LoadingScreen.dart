import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga_executive/constants/Colors.dart' as prefix0;
import 'package:sapawarga_executive/environment/Environment.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: prefix0.Colors.blue,
        body: Center(
            child: Container(
                width: MediaQuery.of(context).size.width / 2,
                child: FlareActor('${Environment.flareAssets}wave.flr',
                    alignment: Alignment.center,
                    fit: BoxFit.contain,
                    animation: "Aura"))));
  }
}
