import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/models/GeneralTitleModel.dart';

abstract class EducationsListState extends Equatable {
  EducationsListState([List props = const []]);
}

class InitialEducationsListState extends EducationsListState {
  @override
  String toString() => 'State InitialEducationsListState';

  @override
  List<Object> get props => [];
}

class EducationsListLoading extends EducationsListState {
  @override
  String toString() => 'State EducationsListLoading';

  @override
  List<Object> get props => [];

}

class EducationsLoaded extends EducationsListState {
  final List<GeneralTitleModel> record;

  EducationsLoaded({@required this.record});

  @override
  String toString() => 'State EducationsLoaded';

  @override
  List<Object> get props => [record];
}

class EducationsFailure extends EducationsListState {
  final String error;

  EducationsFailure({this.error});

  @override
  String toString() {
    return 'EducationsFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
