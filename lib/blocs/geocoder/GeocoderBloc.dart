import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:geocoder/geocoder.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/repositories/GeocoderRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';
import './Bloc.dart';

class GeocoderBloc extends Bloc<GeocoderEvent, GeocoderState> {

  final GeocoderRepository geocoderRepository;

  GeocoderBloc({@required this.geocoderRepository}):assert(geocoderRepository!=null);

  @override
  GeocoderState get initialState => GeocoderStateInitial();

  @override
  Stream<GeocoderState> mapEventToState(
    GeocoderEvent event,
  ) async* {
    if (event is GeocoderGetLocation) {

      yield GeocoderLoading();

      try {

        Address address = await geocoderRepository.getAddress(event.coordinate);

        String primaryAddress;
        String secondaryAddress;

        try {
          if (address.addressLine.toLowerCase().contains("kecamatan")) {
            primaryAddress = address.addressLine.substring(
                0, address.addressLine.toLowerCase().indexOf(", kecamatan"));
          } else {
            primaryAddress = address.addressLine.substring(
                0, address.addressLine.toLowerCase().indexOf(", kec."));
          }
        } catch (_) {
          primaryAddress = address.addressLine;
        }

        try {
          secondaryAddress =
          "${address.locality}, ${address.subAdminArea}, ${address.adminArea}";
        } catch (_) {
          try {
            secondaryAddress = secondaryAddress =
                address.addressLine.replaceAll(primaryAddress + ", ", "");
          } catch (_) {
            secondaryAddress = "";
          }
        }

        yield GeocoderLoaded(
            primaryAddress: primaryAddress, secondaryAddress: secondaryAddress);
      } catch (e) {
        yield GeocoderFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
