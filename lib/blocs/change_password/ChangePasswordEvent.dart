import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangePasswordEvent extends Equatable {
  ChangePasswordEvent([List props = const <dynamic>[]]);
}

class SendChangedPassword extends ChangePasswordEvent {
  final String oldPass;
  final String newPass;
  final String confNewPass;

  SendChangedPassword({@required this.oldPass, @required this.newPass, @required this.confNewPass}):assert((oldPass != null) && (newPass != null) && (confNewPass != null));

  @override
  String toString() {
    return 'Event SendChangedPassword';
  }

  @override
  List<Object> get props => [oldPass, newPass, confNewPass];
}
