import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';
import 'package:sapawarga_executive/utilities/exceptions/ValidationException.dart';

import './Bloc.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {

  final AuthProfileRepository authProfileRepository;


  ChangePasswordBloc({@required this.authProfileRepository}):assert(authProfileRepository != null);

  @override
  ChangePasswordState get initialState => ChangePasswordInitial();

  @override
  Stream<ChangePasswordState> mapEventToState(
    ChangePasswordEvent event,
  ) async* {
    if (event is SendChangedPassword) {
      yield ChangePasswordLoading();

      try {
        await authProfileRepository.changePassword(oldPass: event.oldPass, newPass: event.newPass, confNewPass: event.confNewPass);
        yield ChangePasswordSuccess();
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield ChangePasswordFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
