import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangePasswordState extends Equatable {
  ChangePasswordState([List props = const <dynamic>[]]);
}

class ChangePasswordInitial extends ChangePasswordState {
  @override
  List<Object> get props => [];
}

class ChangePasswordLoading extends ChangePasswordState {
  @override
  String toString() {
    return 'State ChangePasswordLoading';
  }

  @override
  List<Object> get props => [];
}

class ChangePasswordSuccess extends ChangePasswordState {
  @override
  String toString() {
    return 'State ChangePasswordSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends ChangePasswordState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => [errors];
}

class ChangePasswordFailure extends ChangePasswordState {
  final String error;

  ChangePasswordFailure({this.error});

  @override
  String toString() {
    return 'State ChangePasswordFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
