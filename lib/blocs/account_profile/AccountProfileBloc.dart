import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileState.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';


class AccountProfileBloc
    extends Bloc<AccountProfileEvent, AccountProfileState> {
  AuthProfileRepository authProfileRepository = AuthProfileRepository();

  AccountProfileBloc.profile({@required this.authProfileRepository}):assert(authProfileRepository != null);

  @override
  AccountProfileState get initialState => AccountProfileInitial();

  Stream<AccountProfileState> mapEventToState(
      AccountProfileEvent event) async* {
    if (event is AccountProfileLoad) {
      yield AccountProfileLoading();

      try {
        Data record = await authProfileRepository.getUserInfo();

        yield AccountProfileLoaded(record: record);
      } catch (e) {
        yield AccountProfileFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is AccountProfileChanged) {
      yield AccountProfileLoading();

      try {
        await Future.delayed(Duration(seconds: 1));

        Data record =
            await authProfileRepository.getUserInfo(forceFetch: true);

        yield AccountProfileLoaded(record: record);
      } catch (e) {
        yield AccountProfileFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
