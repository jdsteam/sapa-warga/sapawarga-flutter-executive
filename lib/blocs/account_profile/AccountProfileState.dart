import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';

abstract class AccountProfileState extends Equatable {
  AccountProfileState([List props = const []]);
}

class AccountProfileInitial extends AccountProfileState {
  @override
  String toString() => 'State AccountProfileInitial';

  @override
  List<Object> get props => [];
}

class AccountProfileLoading extends AccountProfileState {
  @override
  String toString() => 'State AccountProfileLoading';

  @override
  List<Object> get props => [];
}

class AccountProfileLoaded extends AccountProfileState {
  final Data record;

  AccountProfileLoaded({@required this.record}) : super([record]);

  @override
  String toString() => 'State AccountProfileLoaded';

  @override
  List<Object> get props => [record];
}

class AccountProfileFailure extends AccountProfileState {
  final String error;

  AccountProfileFailure({this.error});

  @override
  String toString() {
    return 'AccountProfileFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
