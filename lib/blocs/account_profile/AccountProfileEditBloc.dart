import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';
import 'package:sapawarga_executive/utilities/exceptions/ValidationException.dart';
import './Bloc.dart';

class AccountProfileEditBloc
    extends Bloc<AccountProfileEditEvent, AccountProfileEditState> {
  final AuthProfileRepository authProfileRepository;

  AccountProfileEditBloc({this.authProfileRepository}):assert(authProfileRepository != null);

  @override
  AccountProfileEditState get initialState => AccountProfileEditInitial();

  Stream<AccountProfileEditState> mapEventToState(
      AccountProfileEditEvent event) async* {
    if (event is AccountProfileEditSubmit) {
      yield AccountProfileEditLoading();

      try {
        await authProfileRepository.updateProfile(event.userInfoModel);
        yield AccountProfileEditUpdated();
      } on ValidationException catch (e) {
        yield AccountProfileEditValidationError(errors: e.errors);
      } catch (e) {
        yield AccountProfileEditFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is AccountProfileEditPhotoSubmit) {
      yield AccountProfileEditPhotoLoading();
      try {
        var _image = await authProfileRepository.updatePhoto(
            event.image, event.userInfoModel);

        yield AccountProfileEditPhotoUpdated(image: _image);
      } catch (e) {
        yield AccountProfileEditFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is CompleteProfileSubmit) {
      yield AccountProfileEditLoading();
      try {
        await authProfileRepository.changeProfile(
            name: event.name,
            email: event.email,
            phone: event.phone,
            address: event.address);
        yield AccountProfileEditUpdated();
      } on ValidationException catch (error) {
        yield AccountProfileEditValidationError(errors: error.errors);
      } catch (e) {
        yield AccountProfileEditFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
