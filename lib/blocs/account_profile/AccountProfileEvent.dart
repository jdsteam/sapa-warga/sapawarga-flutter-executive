import 'package:equatable/equatable.dart';

abstract class AccountProfileEvent extends Equatable {
  AccountProfileEvent([List props = const []]);
}

class AccountProfileLoad extends AccountProfileEvent {
  @override
  String toString() => 'Event AccountProfileLoad';

  @override
  List<Object> get props => [];
}

class AccountProfileChanged extends AccountProfileEvent {
  @override
  String toString() => 'Event AccountProfileChanged';

  @override
  List<Object> get props => [];
}
