import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/models/GeneralTitleModel.dart';

abstract class JoblistState extends Equatable {
  JoblistState([List props = const []]);
}

class InitialJoblistState extends JoblistState {
  @override
  String toString() => 'State InitialJoblistState';

  @override
  List<Object> get props => [];
}

class JobsListLoading extends JoblistState {
  @override
  String toString() => 'State JobsListLoading';

  @override
  List<Object> get props => [];
}

class JobsLoaded extends JoblistState {
  final List<GeneralTitleModel> record;

  JobsLoaded({@required this.record});

  @override
  String toString() => 'State JobsLoaded';

  @override
  List<Object> get props => [record];
}

class JobsFailure extends JoblistState {
  final String error;

  JobsFailure({this.error});

  @override
  String toString() {
    return 'JobsFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
