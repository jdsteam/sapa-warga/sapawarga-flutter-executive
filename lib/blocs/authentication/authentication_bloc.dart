import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga_executive/repositories/userRepository.dart';
import 'package:sapawarga_executive/utilities/sharedPreferences/authPreferences.dart';
import './bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => InitialAuthenticationState();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final bool hasToken = await AuthPreferences.hasToken();

      if (hasToken) {
        final record = await _userRepository.getUserInfo();
        yield Authenticated(record);
      } else {
        yield Unauthenticated();
      }
    }

    if (event is LoggedIn) {
      // AuthPreferences
      yield AuthenticationLoading();
      await AuthPreferences.setToken(event.token);
      try {
        final record = await _userRepository.getUserInfo(forceFetch: true);
        yield Authenticated(record);
      } catch (error) {
        yield AuthenticatedError(errors: error);
      }
    }


    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await _userRepository.unAuthenticate();
      yield Unauthenticated();
    }
  }
}
