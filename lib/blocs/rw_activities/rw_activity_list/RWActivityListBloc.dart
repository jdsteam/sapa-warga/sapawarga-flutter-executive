import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga_executive/models/RWActivityModel.dart';
import 'package:sapawarga_executive/repositories/RWActivityRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';
import '../Bloc.dart';

class RWActivityListBloc
    extends Bloc<RWActivityListEvent, RWActivityListState> {

  final RWActivityRepository repository;

  RWActivityListBloc(this.repository);

  @override
  RWActivityListState get initialState => RWActivityListInitial();

  @override
  Stream<RWActivityListState> mapEventToState(
      RWActivityListEvent event,) async* {
    if (event is RWActivityListLoad) {
      if (event.page == 1) yield RWActivityListLoading();

      try {
        RWActivityModel records = await repository.fetchRecords(
            page: event.page);
        yield RWActivityListLoaded(records);
      } catch (e) {
        yield RWActivityListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is MyRWActivityListLoad) {
      if (event.page == 1) yield RWActivityListLoading();

      try {
        RWActivityModel records = await repository.fetchRecordsMyActivities(
            page: event.page);
        yield RWActivityListLoaded(records);
      } catch (e) {
        yield RWActivityListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is RWActivityListSearch) {
      if (event.page == 1) yield RWActivityListLoading();

      try {
        RWActivityModel records = await repository.searchRecord(keyword: event.keyword,
            page: event.page, isMyActivity: event.isMyActivity);
        yield RWActivityListLoaded(records);
      } catch (e) {
        yield RWActivityListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is RWActivityListLike) {
      try {
        await repository.sendLike(id: event.id);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
