import 'package:equatable/equatable.dart';

abstract class RWActivityListEvent extends Equatable {
  const RWActivityListEvent([List props = const <dynamic>[]]);
}

class RWActivityListLoad extends RWActivityListEvent {
  final int page;

  RWActivityListLoad(this.page):super([page]);

  @override
  String toString() {
    return 'Event RWActivityListLoad{page: $page}';
  }

  @override
  List<Object> get props => [page];

}

class MyRWActivityListLoad extends RWActivityListEvent {
  final int page;

  MyRWActivityListLoad(this.page):super([page]);

  @override
  String toString() {
    return 'Event MyRWActivityListLoad{page: $page}';
  }

  @override
  List<Object> get props => [page];

}

class RWActivityListSearch extends RWActivityListEvent {
  final String keyword;
  final int page;
  final bool isMyActivity;

  RWActivityListSearch(this.keyword, this.page, {this.isMyActivity = false});

  @override
  String toString() {
    return 'Event RWActivityListSearch';
  }

  @override
  List<Object> get props => [];
}


class RWActivityListLike extends RWActivityListEvent {
  final int id;

  RWActivityListLike(this.id):super([id]);

  @override
  String toString() {
    return 'Event RWActivityListLike{id: $id}';
  }

  @override
  List<Object> get props => [id];

}
