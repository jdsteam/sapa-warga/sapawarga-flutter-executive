import 'package:equatable/equatable.dart';
import 'package:sapawarga_executive/models/RWActivityCommentModel.dart';

abstract class RWActivityCommentAddState extends Equatable {
  const RWActivityCommentAddState([List props = const <dynamic>[]]);
}

class RWActivityCommentAddInitial extends RWActivityCommentAddState {
  @override
  List<Object> get props => [];
}

class RWActivityCommentAddLoading extends RWActivityCommentAddState {
  @override
  String toString() {
    return 'State RWActivityCommentAddLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityCommentAdded extends RWActivityCommentAddState {
  final ItemRwActivityComment record;

  RWActivityCommentAdded(this.record) : super([record]);

  @override
  String toString() {
    return 'State RWActivityCommentAdded';
  }

  @override
  List<Object> get props => [record];
}

class RWActivityCommentAddFailure extends RWActivityCommentAddState {
  final String error;

  RWActivityCommentAddFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityCommentAddFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}