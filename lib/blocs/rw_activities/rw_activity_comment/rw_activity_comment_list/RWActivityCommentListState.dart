import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga_executive/models/RWActivityCommentModel.dart';


@immutable
abstract class RWActivityCommentListState extends Equatable {
  const RWActivityCommentListState([List props = const <dynamic>[]]);
}

class RWActivityCommentListInitial extends RWActivityCommentListState {
  @override
  List<Object> get props => [];
}

class RWActivityCommentListLoading extends RWActivityCommentListState {
  @override
  String toString() {
    return 'State RWActivityCommentListLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityCommentListLoaded extends RWActivityCommentListState {
  final RWActivityCommentModel records;

  RWActivityCommentListLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State RWActivityCommentListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class RWActivityCommentListFailure extends RWActivityCommentListState {
  final String error;

  RWActivityCommentListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityCommentListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
