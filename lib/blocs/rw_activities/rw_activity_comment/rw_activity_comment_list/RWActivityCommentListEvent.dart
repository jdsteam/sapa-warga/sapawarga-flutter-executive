import 'package:equatable/equatable.dart';

abstract class RWActivityCommentListEvent extends Equatable {
  const RWActivityCommentListEvent([List props = const <dynamic>[]]);
}

class RWActivityCommentListLoad extends RWActivityCommentListEvent {
  final int id;
  final int page;

  RWActivityCommentListLoad(this.id, this.page):super([page]);


  @override
  String toString() {
    return 'Event RWActivityCommentListLoad{id: $id, page: $page}';
  }

  @override
  List<Object> get props => [page];

}
