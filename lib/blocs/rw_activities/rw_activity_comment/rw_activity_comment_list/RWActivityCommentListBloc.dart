import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga_executive/models/RWActivityCommentModel.dart';
import 'package:sapawarga_executive/repositories/RWActivityRepository.dart';
import 'package:sapawarga_executive/utilities/exceptions/CustomException.dart';
import '../Bloc.dart';

class RWActivityCommentListBloc extends Bloc<RWActivityCommentListEvent, RWActivityCommentListState> {

  final RWActivityRepository repository;

  RWActivityCommentListBloc(this.repository);

  @override
  RWActivityCommentListState get initialState => RWActivityCommentListInitial();

  @override
  Stream<RWActivityCommentListState> mapEventToState(
    RWActivityCommentListEvent event,
  ) async* {
    if (event is RWActivityCommentListLoad) {
      if (event.page == 1) yield RWActivityCommentListLoading();

      try {
        RWActivityCommentModel records = await repository.fetchCommentRecords(id: event.id, page: event.page);
        yield RWActivityCommentListLoaded(records);
      } catch (e) {
        yield RWActivityCommentListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
