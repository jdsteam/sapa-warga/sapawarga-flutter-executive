import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga_executive/components/LoadingScreen.dart';
import 'package:sapawarga_executive/configs/Routes.dart';
import 'package:sapawarga_executive/constants/Navigation.dart';
import 'package:sapawarga_executive/repositories/userRepository.dart';
import 'package:sapawarga_executive/screens/home/homeScreen.dart';
import 'package:sapawarga_executive/screens/login/loginScreen.dart';
import 'package:bloc/bloc.dart';
import 'package:sapawarga_executive/screens/splashScreen/SplashScreen.dart';
import 'package:sapawarga_executive/utilities/simpleDelegate.dart';

import 'blocs/authentication/bloc.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final UserRepository userRepository = UserRepository();
  runApp(
    BlocProvider(
      builder: (context) =>
          AuthenticationBloc(userRepository: userRepository)..add(AppStarted()),
      child: App(userRepository: userRepository),
    ),
  );
}

class App extends StatefulWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Unauthenticated) {
            return LoginScreen(
              userRepository: widget._userRepository,
            );
          } else if (state is Authenticated) {
            return HomeScreen();
          } else if (state is AuthenticationLoading) {
            return LoadingScreen();
          } else {
            return SplashScreen();
          }
        },
      ),
      onGenerateRoute: generateRoutes,
      navigatorKey: NavigationConstrants.navKey,
    );
  }
}
