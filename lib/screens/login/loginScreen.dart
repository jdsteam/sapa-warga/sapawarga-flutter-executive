import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';
import 'package:sapawarga_executive/blocs/authentication/authentication_bloc.dart';
import 'package:sapawarga_executive/blocs/login/bloc.dart';
import 'package:sapawarga_executive/components/TextButton.dart';
import 'package:sapawarga_executive/constants/BrowserScreen.dart';
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/UrlThirdParty.dart';
import 'package:sapawarga_executive/repositories/userRepository.dart';
import 'package:sapawarga_executive/screens/login/loginForm.dart';

class LoginScreen extends StatefulWidget {
  final UserRepository userRepository;

  LoginScreen({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  UserRepository get _userRepository => widget.userRepository;

// TODO: onProgress
  String _versionText = Dictionary.version;

  @override
  void initState() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        _versionText = packageInfo.version != null
            ? packageInfo.version
            : Dictionary.version;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
//      navigationBar: CupertinoNavigationBar(
//        middle: Text('Login'),
//      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider<LoginBloc>(
            builder: (BuildContext context) => LoginBloc(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
              userRepository: _userRepository,
            ),
          )
        ],
        child: _buildContent(),
      ),
    );
  }

  _buildContent() {
    return Stack(
      children: <Widget>[
        // Login Form
        Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 80.0),
            child: LoginForm(),
          ),
        ),

        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(
                    padding: EdgeInsets.all(10.0),
                    title: Dictionary.termOfService,
                    textStyle: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BrowserScreen(
                              url: UrlThirdParty.termOfService,
                            )),
                      );
                    },
                  ),
                  Text(' | '),
                  TextButton(
                    padding: EdgeInsets.all(5.0),
                    title: Dictionary.privacyPolicy,
                    textStyle: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BrowserScreen(
                              url: UrlThirdParty.privacyPolicy,
                            )
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),

            // TODO: Don't delete this code. It's possible to be used again
            /*GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 5.0, bottom: 10.0),
                        child: Text(
                          Dictionary.downloadGuideBook,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      onTap: _downloadGuideBook,
                    ),*/


            Container(
              width: MediaQuery.of(context).size.width,
              height: 1.5,
              color: Color.fromRGBO(180, 180, 180, 0.5),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              color: Colors.white,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
//                    Text('Version ' + _versionText,
//                        style: TextStyle(fontSize: 12.0)),
                    Text(Dictionary.copyRight,
                        style: TextStyle(fontSize: 12.0)),
                  ],
                ),
              ),
            )
          ],
        )
        // Foote
      ],
    );
  }
}
