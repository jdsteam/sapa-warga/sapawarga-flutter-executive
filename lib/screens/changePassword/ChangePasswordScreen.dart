import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga_executive/blocs/authentication/bloc.dart';
import 'package:sapawarga_executive/blocs/change_password/Bloc.dart';
import 'package:sapawarga_executive/components/BlockCircleLoading.dart';
import 'package:sapawarga_executive/components/DialogTextOnly.dart';
import 'package:sapawarga_executive/components/PasswordFormField.dart';
import 'package:sapawarga_executive/components/RoundedButton.dart';
import 'package:sapawarga_executive/components/TextButton.dart';
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/enum/ChangePasswordType.dart';
import 'package:sapawarga_executive/environment/Environment.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/utilities/validations/EditValidation.dart';
import 'package:sapawarga_executive/utilities/validations/LoginValidation.dart';

class ChangePasswordScreen extends StatelessWidget {
  final ChangePasswordType type;

  ChangePasswordScreen({@required this.type}) : assert(type != null);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChangePasswordBloc>(
      builder: (context) =>
          ChangePasswordBloc(authProfileRepository: AuthProfileRepository()),
      child: ChangePassword(type),
    );
  }
}

class ChangePassword extends StatefulWidget {
  final ChangePasswordType type;

  ChangePassword(this.type);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  ChangePasswordBloc _changePasswordBloc;
  AuthenticationBloc _authenticationBloc;
  final _formKey = GlobalKey<FormState>();
  final _oldPassController = TextEditingController();
  final _newPassController = TextEditingController();
  final _confirmPassController = TextEditingController();
  bool _autoValidate = false;
  bool _isLoading = false;

  @override
  void initState() {
    _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChangePasswordBloc, ChangePasswordState>(
      bloc: _changePasswordBloc,
      listener: (context, state) {
        if (state is ChangePasswordLoading) {
          _isLoading = true;
          blockCircleLoading(context: context);
        } else if (state is ChangePasswordSuccess) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: Dictionary.successChangePassword,
                    buttonText: "OK",
                    onOkPressed: () {
                      if (widget.type == ChangePasswordType.force) {
                        _authenticationBloc.add(LoggedOut());
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // To close the dialog
                      } else {
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      }
                    },
                  ));
        } else if (state is ChangePasswordFailure) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.error.toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop(); // To close the dialog
                      },
                    ));
          }
        } else if (state is ValidationError) {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: state.errors['password'][0].toString(),
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context, rootNavigator: true)
                          .pop(); // To close the dialog
                    },
                  ));
        } else {
          if (_isLoading) {
            _isLoading = false;
            Navigator.of(context, rootNavigator: true).pop();
          }
        }
      },
      child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
        bloc: _changePasswordBloc,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(Dictionary.changePassword),
              centerTitle: true,
            ),
            body: WillPopScope(
                onWillPop: _onBackPressed,
                child: Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 20.0),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 65.0),
                            child: SingleChildScrollView(
                              padding: EdgeInsets.only(bottom: 65.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  widget.type == ChangePasswordType.profile
                                      ? _currentPasswordField()
                                      : Container(),
                                  Text(
                                    Dictionary.labelPasswordNew,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  PasswordFormField(
                                    controller: _newPassController,
                                    showIcon: false,
                                    hasFloatingPlaceholder: false,
                                    hintText: Dictionary.hintPasswordNew,
                                    validator: (val) =>
                                        LoginValidations.passwordValidation(val),
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  Text(
                                    Dictionary.labelPasswordNewConfirm,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  PasswordFormField(
                                    controller: _confirmPassController,
                                    showIcon: false,
                                    hasFloatingPlaceholder: false,
                                    hintText: Dictionary.hintPasswordNew,
                                    validator: (val) =>
                                        EditValidation.confirmPasswordValidation(
                                            _newPassController.text, val),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: RoundedButton(
                              title: Dictionary.save,
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.green,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(color: Colors.white),
                              onPressed: () {
                                _onSaveButtonPressed();
                              },
                            ),
                          )
                        ],
                      ),
                    ))),
          );
        },
      ),
    );
  }

  Widget _currentPasswordField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          Dictionary.labelPasswordOld,
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 5.0,
        ),
        PasswordFormField(
          controller: _oldPassController,
          showIcon: false,
          hasFloatingPlaceholder: false,
          hintText: Dictionary.hintPasswordOld,
          validator: (val) => EditValidation.passwordValidation(val),
        ),
        SizedBox(
          height: 10.0,
        ),
        TextButton(
            title: Dictionary.forgotPassword,
            textStyle:
                TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
            onTap: () {}),
        SizedBox(
          height: 30.0,
        ),
      ],
    );
  }

  _onSaveButtonPressed() {
    setState(() {
      _autoValidate = true;
    });

    if (_formKey.currentState.validate()) {
      _changePasswordBloc.add(SendChangedPassword(
          oldPass: widget.type == ChangePasswordType.profile
              ? _oldPassController.text
              : Environment.defaultPassword,
          newPass: _newPassController.text,
          confNewPass: _confirmPassController.text));
    } else {
      print("Validate Error");
    }
  }

  Future<bool> _onBackPressed() async {
    if (widget.type == ChangePasswordType.force) {
      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(Dictionary.confirmExitTitle),
              content: Text(Dictionary.confirmExit),
              actions: <Widget>[
                FlatButton(
                  child: Text(Dictionary.yes),
                  onPressed: () {
                    exit(0);
                  },
                ),
                FlatButton(
                  child: Text(Dictionary.cancel),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
      return false;
    } else {
      return true;
    }
  }

  @override
  void dispose() {
    _changePasswordBloc.close();
    _oldPassController.dispose();
    _newPassController.dispose();
    _confirmPassController.dispose();
    super.dispose();
  }
}
