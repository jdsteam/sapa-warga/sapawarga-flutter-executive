import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga_executive/blocs/rw_activities/rw_activity_comment/Bloc.dart';
import 'package:sapawarga_executive/blocs/rw_activities/rw_activity_comment/rw_activity_comment_add/RWActivityCommentAddBloc.dart';
import 'package:sapawarga_executive/blocs/rw_activities/rw_activity_comment/rw_activity_comment_list/RWActivityCommentListBloc.dart';
import 'package:sapawarga_executive/components/CustomAppBar.dart';
import 'package:sapawarga_executive/components/DialogTextOnly.dart';
import 'package:sapawarga_executive/components/HeroImagePreviewScreen.dart';
import 'package:sapawarga_executive/constants/Colors.dart' as prefix0;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/Dimens.dart';
import 'package:sapawarga_executive/constants/FontsFamily.dart';
import 'package:sapawarga_executive/environment/Environment.dart';
import 'package:sapawarga_executive/models/RWActivityCommentModel.dart';
import 'package:sapawarga_executive/models/RWActivityModel.dart';
import 'package:sapawarga_executive/repositories/RWActivityRepository.dart';
import 'package:sapawarga_executive/screens/rwActivities/RWActivityDetailComments.dart';
import 'package:sapawarga_executive/utilities/FormatDate.dart';

class RWActivityDetailScreen extends StatefulWidget {
  final ItemRWActivity record;

  RWActivityDetailScreen(this.record);

  @override
  _RWActivityDetailScreenState createState() => _RWActivityDetailScreenState();
}

class _RWActivityDetailScreenState extends State<RWActivityDetailScreen> {
  final _textMessage = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _scrollController = ScrollController();
  RefreshController _mainRefreshController = RefreshController();
  List<ItemRwActivityComment> _listComments = List<ItemRwActivityComment>();

  ItemRWActivity get record => widget.record;
  RWActivityCommentAddBloc _addCommentBloc;
  RWActivityCommentListBloc _listCommentBloc;

  bool _isLoaded = false;
  int _pageCount = 0;
  int _currentPage = 1;
  int _itemCount = 0;
  final _scrollThreshold = 200.0;
  String _likeState = '${Environment.imageAssets}like-empty.png';

  @override
  void initState() {

    _scrollController.addListener(() {
      _onScroll();
    });

    _textMessage.addListener((() {
      setState(() {});
    }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar().DefaultAppBar(title: Dictionary.activities),
      body: Container(
        child: MultiBlocProvider(
            providers: [
              BlocProvider<RWActivityCommentListBloc>(
                builder: (context) => _listCommentBloc =
                    RWActivityCommentListBloc(RWActivityRepository())
                      ..add(RWActivityCommentListLoad(record.id, _currentPage)),
              ),
              BlocProvider<RWActivityCommentAddBloc>(
                builder: (context) => _addCommentBloc =
                    RWActivityCommentAddBloc(RWActivityRepository()),
              ),
            ],
            child: MultiBlocListener(
              listeners: [
                BlocListener<RWActivityCommentListBloc,
                    RWActivityCommentListState>(
                  bloc: _listCommentBloc,
                  listener: (context, state) {
                    if (state is RWActivityCommentListLoaded) {
                      _pageCount = state.records.meta.pageCount;
                      _currentPage = state.records.meta.currentPage + 1;
                      _itemCount = state.records.meta.totalCount;
                      _listComments.addAll(state.records.items);
                      _listComments = _listComments.toSet().toList();
                      _isLoaded = true;
                      setState(() {});
                    }
                  },
                ),
                BlocListener<RWActivityCommentAddBloc,
                    RWActivityCommentAddState>(
                  bloc: _addCommentBloc,
                  listener: (context, state) {
                    if (state is RWActivityCommentAdded) {
                      _itemCount = _itemCount + 1;
                      _listComments.insert(0, state.record);

                      setState(() {});

                      _scaffoldKey.currentState.showSnackBar(
                        SnackBar(
                          content: Container(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                Dictionary.successAddComment,
                                style: TextStyle(fontSize: 14.0),
                              )),
                          backgroundColor: prefix0.Colors.blue,
                          duration: Duration(seconds: 2),
                        ),
                      );
                    } else if (state is RWActivityCommentAddFailure) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => DialogTextOnly(
                                description: state.error,
                                buttonText: "OK",
                                onOkPressed: () {
                                  Navigator.of(context)
                                      .pop(); // To close the dialog
                                },
                              ));
                    }
                  },
                ),
              ],
              child: _buildContent(),
            )),
      ),
    );
  }

  Container _buildContent() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: SmartRefresher(
              controller: _mainRefreshController,
              enablePullDown: true,
              header: WaterDropMaterialHeader(),
              onRefresh: () async {
                _isLoaded = false;
                setState(() {});
                _currentPage = 1;
                _listComments.clear();
                _listCommentBloc
                    .add(RWActivityCommentListLoad(record.id, _currentPage));
                _mainRefreshController.refreshCompleted();
              },
              child: ListView(
                controller: _scrollController,
                children: <Widget>[
                  /** Detail description post **/
                  _buildPost(),
                  Divider(thickness: 2.0),

                  /** List of comments **/
                  RWActivityDetailComments(
                      _listCommentBloc, _listComments, _itemCount)
                ],
              ),
            ),
          ),

          /** Input field & send button **/
          _isLoaded
              ? Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(top: BorderSide(color: Colors.grey[300]))),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 50),
                        child: TextField(
                            controller: _textMessage,
                            autofocus: false,
                            maxLines: 5,
                            minLines: 1,
                            maxLength: 255,
                            style:
                                TextStyle(color: Colors.black, fontSize: 16.0),
                            decoration: InputDecoration(
                                hintText: Dictionary.hintComment,
                                counterText: "",
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: Dimens.contentPadding,
                                    top: Dimens.contentPadding,
                                    bottom: Dimens.contentPadding))),
                      ),
                      Positioned(
                          right: 15.0,
                          bottom: 15.0,
                          child: _textMessage.text.trim().isNotEmpty
                              ? GestureDetector(
                                  child:
                                      Icon(Icons.send, color: prefix0.Colors.blue),
                                  onTap: () {
                                    setState(() {
                                      _addCommentBloc.add(
                                        RWActivityCommentAdd(
                                          id: record.id,
                                          text: _textMessage.text.trim(),
                                        ),
                                      );

                                      _textMessage.clear();
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                    });
                                  })
                              : Icon(Icons.send, color: prefix0.Colors.grey))
                    ],
                  ))
              : Container(),
        ],
      ),
    );
  }

  _buildPost() {
    bool isLiked = record.isLiked;

    if (isLiked) {
      _likeState = '${Environment.imageAssets}liked.png';
    } else {
      _likeState = '${Environment.imageAssets}like-empty.png';
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: Dimens.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: CachedNetworkImage(
                  imageUrl: record.user.photoUrlFull,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Container(
                      color: Colors.grey[200],
                      child: Center(child: CupertinoActivityIndicator())),
                  errorWidget: (context, url, error) => Container(
                      color: Colors.grey[200],
                      child: Image.asset('${Environment.imageAssets}user.png',
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  record.user.name,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: FontsFamily.productSans),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'RW ' +
                      record.user.rw +
                      ', ' +
                      record.user.kelurahan +
                      ', ' +
                      record.user.kecamatan +
                      ', ' +
                      record.user.kabkota,
                  style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: GestureDetector(
                child: Hero(
                  tag: Dictionary.heroImageTag,
                  child: CachedNetworkImage(
                    imageUrl: record.imagePathFull,
                    fit: BoxFit.fitWidth,
                    placeholder: (context, url) => Center(
                        heightFactor: 8.2, child: CupertinoActivityIndicator()),
                    errorWidget: (context, url, error) => Container(
                        height: MediaQuery.of(context).size.height / 3.3,
                        color: Colors.grey[200],
                        child: Image.asset(
                            '${Environment.imageAssets}placeholder.png',
                            fit: BoxFit.fitWidth)),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => HeroImagePreview(
                              Dictionary.heroImageTag, record.imagePathFull)));
                },
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 2.0),
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(
                            _likeState,
                          ),
                        ),
                        onTap: () {
                          List<ItemRWActivity> listRecords = [];
                          listRecords.add(record);
                          if (isLiked) {
                            _updateLike(listRecords, record.id, true);
                          } else {
                            _updateLike(listRecords, record.id, false);
                          }
                        },
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        record.likesCount > 0 ? '${record.likesCount}' : '',
                        style: TextStyle(fontSize: 15.0),
                      ),
                      SizedBox(width: 5.0),
                      Container(
                        margin: EdgeInsets.only(right: 2.0),
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              '${Environment.iconAssets}comments.png',
                              width: 24.0,
                              height: 24.0,
                            ),
                            record.commentsCount > 0
                                ? SizedBox(width: 5.0)
                                : Container(),
                            Text(
                              record.commentsCount > 0
                                  ? '${record.commentsCount}'
                                  : '',
                              style: TextStyle(fontSize: 15.0),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  unixTimeStampToTimeAgo(record.createdAt),
                  style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                )
              ],
            ),
          ),
          record.status == 0
              ? Container(
                  margin: EdgeInsets.only(top: 15.0),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        '${Environment.iconAssets}warning_rw.png',
                        width: 18.0,
                        height: 18.0,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 5),
                        width: MediaQuery.of(context).size.width - 120,
                        child: Text(
                          Dictionary.contentNotPermittedRWActivities,
                          style: TextStyle(fontSize: 15.0, color: Colors.red),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ))
              : SizedBox(height: 15.0),
          Container(
            margin: EdgeInsets.only(top: 5.0),
            child: Text(
              record.text,
              style: TextStyle(fontSize: 15.0),
            ),
          ),
        ],
      ),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        _listCommentBloc
            .add(RWActivityCommentListLoad(record.id, _currentPage));
      }
    }
  }

  _updateLike(List<ItemRWActivity> records, int id, bool isLike) {
    for (int i = 0; i < records.length; i++) {
      if (records[i].id == id) {
        if (isLike) {
          if (records[i].isLiked) {
            setState(() {
              records[i].isLiked = false;
              records[i].likesCount = records[i].likesCount - 1;
            });
          }
        } else {
          setState(() {
            records[i].likesCount = records[i].likesCount + 1;
            records[i].isLiked = true;
          });
        }
      }
    }
  }

  @override
  void dispose() {
    _textMessage.dispose();
    _addCommentBloc.close();
    super.dispose();
  }
}
