import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga_executive/blocs/authentication/authentication_bloc.dart';
import 'package:sapawarga_executive/blocs/authentication/authentication_event.dart';
import 'package:sapawarga_executive/blocs/rw_activities/Bloc.dart';
import 'package:sapawarga_executive/blocs/rw_activities/rw_activity_list/RWActivityListBloc.dart';
import 'package:sapawarga_executive/components/DialogRequestPermission.dart';
import 'package:sapawarga_executive/components/EmptyData.dart';
import 'package:sapawarga_executive/components/ReadMoreText.dart';
import 'package:sapawarga_executive/components/Skeleton.dart';
import 'package:sapawarga_executive/constants/BrowserScreen.dart';
import 'package:sapawarga_executive/constants/Colors.dart' as prefix0;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/Dimens.dart';
import 'package:sapawarga_executive/constants/ErrorContent.dart';
import 'package:sapawarga_executive/constants/FontsFamily.dart';
import 'package:sapawarga_executive/constants/UrlThirdParty.dart';
import 'package:sapawarga_executive/environment/Environment.dart';
import 'package:sapawarga_executive/models/RWActivityModel.dart';
import 'package:sapawarga_executive/repositories/RWActivityRepository.dart';
import 'package:sapawarga_executive/screens/rwActivities/RWActivityDetailScreen.dart';
import 'package:sapawarga_executive/utilities/CustomPageRoute.dart';
import 'package:sapawarga_executive/utilities/FormatDate.dart';

// ignore: must_be_immutable
class RWActivityListScreen extends StatefulWidget {
  bool tooltipLoad;

  @override
  _RWActivityListScreenState createState() => _RWActivityListScreenState();
}

class _RWActivityListScreenState extends State<RWActivityListScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _mainRefreshController = RefreshController();
  ScrollController _scrollController = ScrollController();
  List<ItemRWActivity> _rwActivityList = List<ItemRWActivity>();

  RWActivityListBloc _listBloc;
  AuthenticationBloc _authenticationBloc;

  String _likeState = '${Environment.imageAssets}like-empty.png';

  BuildContext showcaseContext;
  int _pageCount = 0;
  int _currentPage = 1;
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    _scrollController.addListener(() {
      _onScroll();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
          child: BlocProvider<RWActivityListBloc>(
        builder: (context) {
          return _listBloc = RWActivityListBloc(RWActivityRepository())
            ..add(RWActivityListLoad(_currentPage));
        },
        child: _buildBlocListener(),
      )),
    );
  }

  _buildBlocListener() {
    return BlocListener<RWActivityListBloc, RWActivityListState>(
      bloc: _listBloc,
      listener: (context, state) {
        if (state is RWActivityListLoaded) {
          _pageCount = state.records.meta.pageCount;
          _currentPage = state.records.meta.currentPage + 1;
          _rwActivityList.addAll(state.records.items);
          _rwActivityList = _rwActivityList.toSet().toList();
          setState(() {});
        } else if (state is RWActivityListFailure) {
          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
            Navigator.of(context).pop();
          }
        }
      },
      child: BlocBuilder<RWActivityListBloc, RWActivityListState>(
        bloc: _listBloc,
        builder: (context, state) => SmartRefresher(
            controller: _mainRefreshController,
            enablePullDown: true,
            header: WaterDropMaterialHeader(),
            onRefresh: refresh,
            child: state is RWActivityListLoading
                ? _buildLoading()
                : state is RWActivityListLoaded
                    ? state.records.items.isNotEmpty
                        ? _buildContent(state)
                        : EmptyData(message: Dictionary.emptyDataRWActivity)
                    : state is RWActivityListFailure
                        ? ErrorContent(error: state.error)
                        : _buildLoading()),
      ),
    );
  }

  _buildLoading() {
    return ListView.separated(
      padding: EdgeInsets.only(bottom: 80.0),
      itemCount: 2,
      separatorBuilder: (context, index) => Container(
          width: MediaQuery.of(context).size.width,
          height: 1.0,
          color: Colors.grey[300]),
      itemBuilder: (context, index) => Skeleton(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Container(
                        width: 40.0, height: 40.0, color: Colors.grey[300]),
                  ),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 2,
                        height: 20.0,
                        color: Colors.grey[300]),
                    SizedBox(height: 2.0),
                    Container(
                        width: 50.0, height: 15.0, color: Colors.grey[300]),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 3.5,
                      color: Colors.grey[300]),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 20,
                            height: 20,
                            color: Colors.grey[300],
                          ),
                          SizedBox(width: 5.0),
                          Container(
                              width: MediaQuery.of(context).size.width / 4,
                              height: 20.0,
                              color: Colors.grey[300]),
                        ],
                      ),
                    ),
                    Container(
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 20,
                            height: 20,
                            color: Colors.grey[300],
                          ),
                          SizedBox(width: 5.0),
                          Container(
                              width: MediaQuery.of(context).size.width / 4,
                              height: 20.0,
                              color: Colors.grey[300]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 10.0),
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: MediaQuery.of(context).size.width,
                  height: 20.0,
                  color: Colors.grey[300]),
              Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: MediaQuery.of(context).size.width / 1.5,
                  height: 20.0,
                  color: Colors.grey[300]),
            ],
          ),
        ),
      ),
    );
  }

  _buildContent(RWActivityListLoaded state) {
    return ListView.separated(
        controller: _scrollController,
        padding: EdgeInsets.only(bottom: 80.0),
        itemCount: state.records.meta.totalCount == _rwActivityList.length
            ? _rwActivityList.length
            : _rwActivityList.length + 1,
        separatorBuilder: (context, index) => Container(
            width: MediaQuery.of(context).size.width,
            height: 1.0,
            color: Colors.grey[300]),
        itemBuilder: (context, index) => index >= _rwActivityList.length
            ? Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Column(
                  children: <Widget>[
                    CupertinoActivityIndicator(),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(Dictionary.loadingData),
                  ],
                ),
              )
            : _buildItem(context, index));
  }

  GestureDetector _buildItem(BuildContext context, int index) {
    bool isLiked = _rwActivityList[index].isLiked;

    if (isLiked) {
      _likeState = '${Environment.imageAssets}liked.png';
    } else {
      _likeState = '${Environment.imageAssets}like-empty.png';
    }

    return GestureDetector(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding:
            EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              leading: Container(
                width: 40.0,
                height: 40.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: CachedNetworkImage(
                    imageUrl: _rwActivityList[index].user.photoUrlFull,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Container(
                        color: Colors.grey[200],
                        child: Center(child: CupertinoActivityIndicator())),
                    errorWidget: (context, url, error) => Container(
                        color: Colors.grey[200],
                        child: Image.asset('${Environment.imageAssets}user.png',
                            fit: BoxFit.cover)),
                  ),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _rwActivityList[index].user.name,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.productSans),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'RW ' +
                        _rwActivityList[index].user.rw +
                        ', ' +
                        _rwActivityList[index].user.kelurahan,
                    style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  )
                ],
              ),
            ),
            _rwActivityList[index].status == 0 ? _pendingMarker() : Container(),
            Container(
              margin: EdgeInsets.only(top: 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: CachedNetworkImage(
                  imageUrl: _rwActivityList[index].imagePathFull,
                  fit: BoxFit.fitWidth,
                  placeholder: (context, url) => Center(
                      heightFactor: 8.2, child: CupertinoActivityIndicator()),
                  errorWidget: (context, url, error) => Container(
                      height: MediaQuery.of(context).size.height / 3.3,
                      color: Colors.grey[200],
                      child: Image.asset(
                          '${Environment.imageAssets}placeholder.png',
                          fit: BoxFit.fitWidth)),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 2.0),
                          child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                child: Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    _likeState,
                                  ),
                                ),
                                onTap: () {
                                  _listBloc.add(RWActivityListLike(
                                      _rwActivityList[index].id));
                                  if (isLiked) {
                                    _updateLike(_rwActivityList,
                                        _rwActivityList[index].id, true);
                                  } else {
                                    _updateLike(_rwActivityList,
                                        _rwActivityList[index].id, false);
                                  }
                                },
                              ),
                              SizedBox(width: 5.0),
                              Text(
                                _rwActivityList[index].likesCount > 0
                                    ? '${_rwActivityList[index].likesCount}'
                                    : '',
                                style: TextStyle(fontSize: 15.0),
                              ),
                              SizedBox(width: 5.0),
                              Container(
                                margin: EdgeInsets.only(right: 2.0),
                                child: Wrap(
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      '${Environment.iconAssets}comments.png',
                                      width: 24.0,
                                      height: 24.0,
                                    ),
                                    _rwActivityList[index].commentsCount > 0
                                        ? SizedBox(width: 5.0)
                                        : Container(),
                                    Text(
                                      _rwActivityList[index].commentsCount > 0
                                          ? '${_rwActivityList[index].commentsCount}'
                                          : '',
                                      style: TextStyle(fontSize: 15.0),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Text(
                      unixTimeStampToTimeAgo(_rwActivityList[index].createdAt),
                      style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                    )
                  ],
                )),
            _rwActivityList[index].status == 0
                ? Container(
                    margin: EdgeInsets.only(top: 15.0),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          '${Environment.iconAssets}warning_rw.png',
                          width: 18.0,
                          height: 18.0,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          width: MediaQuery.of(context).size.width - 120,
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                text:
                                    Dictionary.contentNotPermittedRWActivities,
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.red),
                              ),
                              TextSpan(
                                  text: Dictionary.readMore,
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => BrowserScreen(
                                            url: UrlThirdParty
                                                .urlCommunityGuideLine,
                                          ),
                                        ),
                                      );
                                    })
                            ]),
                          ),
                        )
                      ],
                    ))
                : SizedBox(height: 15.0),
            Container(
                margin: EdgeInsets.only(top: 10.0),
                child: ReadMoreText(_rwActivityList[index].text,
                    style: TextStyle(fontSize: 15.0),
                    trimLines: 3,
                    colorClickableText: Colors.grey[600],
                    trimMode: TrimMode.Line,
                    trimCollapsedText: '... Selengkapnya')),
            _rwActivityList[index].lastComment != null
                ? Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                            _rwActivityList[index]
                                .lastComment
                                .user
                                .name
                                .split(' ')[0],
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey[900])),
                        _rwActivityList[index].lastComment.user.roleLabel == 'pimpinan'
                            ? Icon(
                                Icons.check_circle,
                                color: prefix0.Colors.blue,
                                size: 18.0,
                              )
                            : Container(),
                        RichText(
                          maxLines: 3,
                          overflow: TextOverflow.fade,
                          text: TextSpan(
                            children: <TextSpan>[
//                              TextSpan(
//                                  text: _rwActivityList[index]
//                                      .lastComment
//                                      .user
//                                      .name
//                                      .split(' ')[0],
//                                  style: TextStyle(
//                                      fontWeight: FontWeight.bold,
//                                      color: Colors.grey[900])),
                              TextSpan(text: ' '),
                              TextSpan(
                                text: _rwActivityList[index].lastComment.text,
                                style: TextStyle(color: Colors.grey[900]),
                              ),
                            ],
                          ),
                        )
                      ],
                    ))
                : Container(),
          ],
        ),
      ),
      onTap: () {
        Navigator.of(context).push(
            SizedPageRoute(RWActivityDetailScreen(_rwActivityList[index])));
      },
    );
  }

  Stack _pendingMarker() {
    return Stack(
      children: <Widget>[
        Container(
            height: 30,
            child: Image.asset(
                '${Environment.imageAssets}ribbon-silhouette-leading.png',
                color: Colors.amber)),
        Container(
          margin: EdgeInsets.only(left: 70.0, top: 0.18),
          width: 30,
          height: 24.75,
          color: Colors.amber,
        ),
        Container(
            margin: EdgeInsets.only(left: 85),
            height: 30,
            child: Image.asset(
                '${Environment.imageAssets}ribbon-silhouette-trailing.png',
                color: Colors.amber)),
        Container(
          margin: EdgeInsets.only(left: 10, top: 2),
          child: Text(
            Dictionary.postRWActivitiesRejected,
            style: TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }

  _updateLike(List<ItemRWActivity> records, int id, bool isLike) {
    for (int i = 0; i < records.length; i++) {
      if (records[i].id == id) {
        if (isLike) {
          if (records[i].isLiked) {
            setState(() {
              records[i].isLiked = false;
              records[i].likesCount = records[i].likesCount - 1;
            });
          }
        } else {
          setState(() {
            records[i].likesCount = records[i].likesCount + 1;
            records[i].isLiked = true;
          });
        }
      }
    }
  }

  Future<void> refresh() async {
    _currentPage = 1;
    _rwActivityList.clear();
    _listBloc.add(RWActivityListLoad(_currentPage));
    _mainRefreshController.refreshCompleted();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        _listBloc.add(RWActivityListLoad(_currentPage));
      }
    }
  }

//  void _openCreatePost() async {
//    File _image = await ImagePickerHelper(context).openDialog();
//
//    if (_image != null) {
//      final result = await Navigator.of(context)
//              .push(FadedPageRoute(RWActivityCreateScreen(_image)))
//          as ItemRWActivity;
//
//      if (result != null) {
//        _scaffoldKey.currentState.showSnackBar(
//          SnackBar(
//            content: Container(
//                padding: EdgeInsets.symmetric(vertical: 10.0),
//                child: Text(
//                  'Kegiatan berhasil diposting',
//                  style: TextStyle(fontSize: 14.0),
//                )),
//            backgroundColor: clr.Colors.blue,
//            duration: Duration(seconds: 2),
//          ),
//        );
//
//        await _mainRefreshController.requestRefresh();
//      }
//    }
//  }

  void _permission() async {
    PermissionStatus permissionStorage = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    PermissionStatus permissionCamera =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permissionStorage != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionRWActivities,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler()
                      .requestPermissions([PermissionGroup.storage]).then(
                          _onStatusRequestedStorage);
                },
              ));
    } else {
      if (permissionCamera != PermissionStatus.granted) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogRequestPermission(
                  image: Image.asset(
                    'assets/icons/photo-camera.png',
                    fit: BoxFit.contain,
                    color: Colors.white,
                  ),
                  description: Dictionary.permissionRWActivities,
                  onOkPressed: () {
                    Navigator.of(context).pop();
                    PermissionHandler()
                        .requestPermissions([PermissionGroup.camera]).then(
                            _onStatusRequestedCamera);
                  },
                ));
      } else {
//        _openCreatePost();
      }
    }
  }

  void _onStatusRequestedStorage(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      PermissionHandler().requestPermissions([PermissionGroup.camera]).then(
          _onStatusRequestedCamera);
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permission();
    }
  }

  @override
  void dispose() {
    _mainRefreshController.dispose();
    _scrollController.dispose();
    _listBloc.close();
    super.dispose();
  }
}
