import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:sapawarga_executive/constants/Colors.dart' as prefix0;
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/FontsFamily.dart';
import 'package:sapawarga_executive/screens/account/ProfileScreen.dart';
import 'package:sapawarga_executive/screens/rwActivities/RWActivityListScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    RWActivityListScreen(),
    ProfileScreen(),
  ];

  @override
  void initState() {
    initializeDateFormatting();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: prefix0.Colors.blue,
        title: Row(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                  Dictionary.appName,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    fontFamily: FontsFamily.intro,
                  ),
                ))
          ],
        ),
//        actions: <Widget>[
//          Container(
//              width: MediaQuery.of(context).size.width / 3,
//              child: IconButton(
//                icon: Icon(Icons.notifications_active,
//                    size: 25.0, color: Colors.white),
//                onPressed: () {
//
//                },
//              )
//          ),
//        ],
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Beranda'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('Akun'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: prefix0.Colors.blue,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
