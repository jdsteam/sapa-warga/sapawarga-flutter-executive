import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileState.dart';
import 'package:sapawarga_executive/blocs/authentication/authentication_bloc.dart';
import 'package:sapawarga_executive/blocs/authentication/authentication_event.dart';
import 'package:sapawarga_executive/components/BuildTextField.dart';
import 'package:sapawarga_executive/components/CustomAppBar.dart';
import 'package:sapawarga_executive/components/DialogTextOnly.dart';
import 'package:sapawarga_executive/components/EmptyData.dart';
import 'package:sapawarga_executive/components/Skeleton.dart';
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/ErrorContent.dart';
import 'package:sapawarga_executive/constants/FontsFamily.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/screens/account/subeditprofile/SubEditContactScreen.dart';
import 'package:sapawarga_executive/utilities/validations/EditValidation.dart';

class SubContactScreen extends StatefulWidget {
  @override
  _SubContactScreenState createState() => _SubContactScreenState();
}

class _SubContactScreenState extends State<SubContactScreen> {
  Data userInfoModel;
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _instagramController = TextEditingController();
  final _facebookController = TextEditingController();
  final _twitterController = TextEditingController();
  AuthenticationBloc _authenticationBloc;
  AccountProfileBloc _accountProfileBloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var  result;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _mainRefreshController = RefreshController();

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: CustomAppBar().DefaultAppBar(title: 'Kontak', actions: <Widget>[
          GestureDetector(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Ubah',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: FontsFamily.intro),
              ),
            ),
            onTap: () async {
              _navigateResult(context);
            },
          )
        ]),
        key: _scaffoldKey,
        body: Container(
            child: BlocProvider<AccountProfileBloc>(
              builder: (context) => _accountProfileBloc = AccountProfileBloc.profile(
                  authProfileRepository: AuthProfileRepository())
                ..add(AccountProfileLoad()),
              child: BlocListener<AccountProfileBloc, AccountProfileState>(
                bloc: _accountProfileBloc,
                listener: (context, state) {
                  if (state is AccountProfileLoaded) {
                    userInfoModel = state.record;

                    _emailController.text = state.record.email;
                    _phoneController.text = state.record.phone;
                    _instagramController.text = state.record.instagram;
                    _facebookController.text = state.record.facebook;
                    _twitterController.text = state.record.twitter;

                    setState(() {});
                  } else if (state is AccountProfileFailure) {
                    if (state.error.contains(Dictionary.errorUnauthorized)) {
                      _authenticationBloc.add(LoggedOut());
                      Navigator.of(context).pop();
                    }
                  }
                },
                child: BlocBuilder<AccountProfileBloc, AccountProfileState>(
                  bloc: _accountProfileBloc,
                  builder: (context, state) => SmartRefresher(
                      controller: _mainRefreshController,
                      enablePullDown: true,
                      header: WaterDropMaterialHeader(),
                      onRefresh: () async {
                        _accountProfileBloc.add(AccountProfileLoad());
                        _mainRefreshController.refreshCompleted();
                      },
                      child: state is AccountProfileLoading
                          ? _buildLoading()
                          : state is AccountProfileLoaded
                          ? state.record != null
                          ? _buildContent(state)
                          : EmptyData(message: Dictionary.emptyDataRWActivity)
                          : state is AccountProfileFailure
                          ? ErrorContent(error: state.error)
                          : _buildLoading()),
                ),
              ),
            )),
      ),
    );

  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context, result);
    return false;
  }

  _buildLoading() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
            ],
          ),
        )
      ],
    );
  }

  _navigateResult(BuildContext context) async {
     result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            SubEditContactScreen(authUserInfo: userInfoModel),
      ),
    );
    _accountProfileBloc.add(AccountProfileLoad());
    if (result == Dictionary.successSaveProfile) {
      await showDialog(
        context: context,
        builder: (BuildContext context) => DialogTextOnly(
          title: Dictionary.congratulation,
          description: Dictionary.successSaveProfile,
          buttonText: Dictionary.ok,
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ),
      );
    }
  }

  _buildContent(AccountProfileLoaded state) {
    return   ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                autovalidate: false,
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.email,
                      hintText: Dictionary.placeHolderEmail,
                      controller: _emailController,
                      validation: EditValidation.emailValidation,
                      textInputType: TextInputType.emailAddress,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.noTelp,
                      hintText: Dictionary.placeHolderNoTelp,
                      controller: _phoneController,
                      validation: EditValidation.phoneValidation,
                      textInputType: TextInputType.phone,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.twitter,
                      hintText: Dictionary.placeHolderTwitter,
                      controller: _twitterController,
                      validation: EditValidation.twitterlidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.instagram,
                      hintText: Dictionary.placeHolderInstagram,
                      controller: _instagramController,
                      validation: EditValidation.instagramValidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.facebook,
                      hintText: Dictionary.placeHolderFacebook,
                      controller: _facebookController,
                      validation: EditValidation.facebooklidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
