import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileEditBloc.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileEditEvent.dart';
import 'package:sapawarga_executive/blocs/account_profile/AccountProfileEditState.dart';
import 'package:sapawarga_executive/components/BuildTextField.dart';
import 'package:sapawarga_executive/components/CustomAppBar.dart';
import 'package:sapawarga_executive/components/DialogRequestPermission.dart';
import 'package:sapawarga_executive/components/DialogTextOnly.dart';
import 'package:sapawarga_executive/components/LocationPicker.dart';
import 'package:sapawarga_executive/constants/Dictionary.dart';
import 'package:sapawarga_executive/constants/FontsFamily.dart';
import 'package:sapawarga_executive/environment/Environment.dart';
import 'package:sapawarga_executive/models/UserInfoModel.dart';
import 'package:sapawarga_executive/repositories/AuthProfileRepository.dart';
import 'package:sapawarga_executive/repositories/EducationRepository.dart';
import 'package:sapawarga_executive/repositories/JobRepository.dart';
import 'package:sapawarga_executive/utilities/Connection.dart';
import 'package:sapawarga_executive/utilities/validations/EditValidation.dart';

class SubEditAddressScreen extends StatelessWidget {
  final Data authUserInfo;
  final AuthProfileRepository authProfileRepository = AuthProfileRepository();
  final EducationRepository educationRepository = EducationRepository();
  final JobRepository jobRepository = JobRepository();

  SubEditAddressScreen({@required this.authUserInfo});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountProfileEditBloc>(
          builder: (context) => AccountProfileEditBloc(
              authProfileRepository: authProfileRepository),
        ),
      ],
      child: SubEditContact(
        userInfoModel: authUserInfo,
      ),
    );
  }
}

class SubEditContact extends StatefulWidget {
  final Data userInfoModel;

  SubEditContact({this.userInfoModel});

  @override
  _SubEditContactState createState() => _SubEditContactState();
}

class _SubEditContactState extends State<SubEditContact> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _addressController = TextEditingController();
  final _kabkotaController = TextEditingController();
  final _kecamatanController = TextEditingController();
  final _kelurahanController = TextEditingController();
  final _rtController = TextEditingController();
  final _rwController = TextEditingController();
  String location = Dictionary.setYourLocation;
  String latitude;
  String longitude;

  bool _autoValidate = false;
  bool isShowDialog = false;
  ProgressDialog loadingDialog;
  GoogleSignIn _googleSignIn;
  AccountProfileEditBloc _blocProfile;
  DateTime _birthDate;

  @override
  void initState() {
    _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    _googleSignIn.signInSilently();
    _addressController.text = widget.userInfoModel.address;
    _kabkotaController.text = widget.userInfoModel.kabkota.name;
    _kecamatanController.text = widget.userInfoModel.kecamatan.name;
    _kelurahanController.text = widget.userInfoModel.kelurahan.name;
    _rtController.text = widget.userInfoModel.rt;
    _rwController.text = widget.userInfoModel.rw;

    location = (widget.userInfoModel.lat != null &&
            widget.userInfoModel.lon != null)
        ? 'Latitude: ${widget.userInfoModel.lat}  Longitude: ${widget.userInfoModel.lon}'
        : Dictionary.setYourLocation;

    _blocProfile = BlocProvider.of<AccountProfileEditBloc>(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _blocProfile,
      listener: (context, state) {
        if (state is AccountProfileEditFailure) {
          showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: state.error,
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                  ));
        }

        if (state is AccountProfileEditValidationError) {
          if (state.errors.containsKey('email')) {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.errors['email'][0].toString(),
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                    ));
          }
        }
      },
      child: BlocBuilder(
          bloc: _blocProfile,
          builder: (BuildContext context, AccountProfileEditState state) {
            if (state is AccountProfileEditUpdated) {
              hideLoading();
              Navigator.pop(context, Dictionary.successSaveProfile);
            }

            if (state is AccountProfileEditPhotoLoading) {
              _onWidgetDidBuild(() {
                showLoading();
              });
            }

            if (state is AccountProfileEditPhotoUpdated) {
              hideLoading();
              _onWidgetDidBuild(() {
                if (isShowDialog) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(Dictionary.updatePhotoTitle),
                          content: Text(Dictionary.successSavePhoto),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(Dictionary.ok),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        );
                      });
                  isShowDialog = false;
                }
              });
            }

            if (state is AccountProfileEditLoading) {
              _onWidgetDidBuild(() {
                showLoading();
              });
            }

            return Scaffold(
              appBar: CustomAppBar().DefaultAppBar(title: 'Ubah Alamat'),
              body: ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Form(
                          key: _formKey,
                          autovalidate: _autoValidate,
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 16.0, right: 16.0, top: 10.0),
                                  alignment: Alignment.topLeft,
                                  child: Text(location)),
                              SizedBox(height: 10),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: ButtonTheme(
                                  minWidth: MediaQuery.of(context).size.width,
                                  height: 45.0,
                                  child: OutlineButton(
                                    child: Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: <Widget>[
                                        Image.asset(
                                          '${Environment.iconAssets}location.png',
                                          width: 24.0,
                                          height: 24.0,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 10.0),
                                          child: Text(
                                            Dictionary.setLocation,
                                            style: TextStyle(
                                                color: Colors.grey[800]),
                                          ),
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      //Color of the border
                                      style: BorderStyle.solid,
                                      //Style of the border
                                      width: 1, //width of the border
                                    ),
                                    onPressed: _handleLocation,
                                  ),
                                ),
                              ),
                              SizedBox(height: 10),
                              BuildTextField(
                                title: Dictionary.fullAddress,
                                hintText: Dictionary.placeHolderAddress,
                                controller: _addressController,
                                validation: EditValidation.addressValidation,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.kabkota,
                                hintText: '',
                                controller: _kabkotaController,
                                validation: null,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                isEdit: false,
                              ),
                              SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.kecamatan,
                                hintText: '',
                                controller: _kecamatanController,
                                validation: null,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                isEdit: false,
                              ),
                              SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.kelurahan,
                                hintText: '',
                                controller: _kelurahanController,
                                validation: null,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                isEdit: false,
                              ),
                              SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.rt.toUpperCase(),
                                hintText: Dictionary.placeHolderRt,
                                controller: _rtController,
                                validation: EditValidation.rtValidation,
                                textInputType: TextInputType.number,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.rw.toUpperCase(),
                                hintText: '',
                                controller: _rwController,
                                validation: null,
                                textInputType: TextInputType.number,
                                textStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                isEdit: false,
                              ),
                              SizedBox(height: 20),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: Material(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: Colors.blue,
                                  child: MaterialButton(
                                    padding: EdgeInsets.all(0),
                                    minWidth: MediaQuery.of(context).size.width,
                                    child: Text(Dictionary.saveProfile,
                                        style: TextStyle(
                                            fontFamily: FontsFamily.productSans,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18.0)),
                                    onPressed: _onSaveProfileButtonPressed,
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }

  // TODO: harus dibuat component supaya bisa di reuse
  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Silahkan Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {});
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  _onSaveProfileButtonPressed() async {
    if (_formKey.currentState.validate()) {
      try {
        bool isConnected =
            await Connection().checkConnection('https://www.google.com');
        if (isConnected) {
          _blocProfile.add(
            AccountProfileEditSubmit(
              userInfoModel: Data(
                name: widget.userInfoModel.name,
                username: widget.userInfoModel.username,
                educationLevelId: widget.userInfoModel.educationLevelId,
                jobTypeId: widget.userInfoModel.jobTypeId,
                email: widget.userInfoModel.email,
                address: _addressController.text,
                lat: latitude,
                lon: longitude,
                phone: widget.userInfoModel.phone,
                rt: _rtController.text,
                facebook: widget.userInfoModel.facebook,
                instagram: widget.userInfoModel.instagram,
                twitter: widget.userInfoModel.twitter,
                birthDate: _birthDate,
              ),
            ),
          );

        }
      } catch (_) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogTextOnly(
                  description: Dictionary.errorConnection,
                  buttonText: "OK",
                  onOkPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                ));
      }
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Future<void> _handleLocation() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.granted) {
      final result = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => LocationPicker()));

      setState(() {
        if (result != null) {
          LatLng latLng = result;
          latitude = '${latLng.latitude.toStringAsFixed(7)}';
          longitude = '${latLng.longitude.toStringAsFixed(7)}';
          location = 'Latitude : $latitude Longitude : $longitude';
        }
      });
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionLocationMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }

  void _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final statusLocation = statuses[PermissionGroup.location];
    if (statusLocation == PermissionStatus.granted) {
      final result = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => LocationPicker()));

      setState(() {
        if (result != null) {
          LatLng latLng = result;
          latitude = '${latLng.latitude.toStringAsFixed(7)}';
          longitude = '${latLng.longitude.toStringAsFixed(7)}';
          location = 'Latitude : $latitude Longitude : $longitude';
        }
      });
    }
  }

  @override
  void dispose() {
    _addressController.dispose();
    _kabkotaController.dispose();
    _kecamatanController.dispose();
    _kelurahanController.dispose();
    _rtController.dispose();
    _rwController.dispose();

    _blocProfile.close();

    super.dispose();
  }
}
