import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:sapawarga_executive/models/UserModel.dart';

import 'MetaResponseModel.dart';


RWActivityModel rwActivityFromJson(String str) => RWActivityModel.fromJson(json.decode(str));
ItemRWActivity itemRWActivityFromJson(String str) => ItemRWActivity.fromJson(json.decode(str));

class RWActivityModel {
  List<ItemRWActivity> items;
  MetaResponseModel meta;

  RWActivityModel({
    this.items,
    this.meta,
  });

  factory RWActivityModel.fromJson(Map<String, dynamic> json) => RWActivityModel(
    items: List<ItemRWActivity>.from(json["items"].map((x) => ItemRWActivity.fromJson(x))),
    meta: MetaResponseModel.fromJson(json["_meta"]),
  );

  Map<String, dynamic> toJson() => {
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
    "_meta": meta.toJson(),
  };
}

// ignore: must_be_immutable
class ItemRWActivity extends Equatable {
  int id;
  String text;
  String imagePathFull;
  int likesCount;
  int commentsCount;
  int lastUserPostCommentId;
  LastComment lastComment;
  int status;
  String statusLabel;
  int createdAt;
  int updatedAt;
  int createdBy;
  bool isLiked;
  User user;

  ItemRWActivity({
    this.id,
    this.text,
    this.imagePathFull,
    this.likesCount,
    this.commentsCount,
    this.lastUserPostCommentId,
    this.lastComment,
    this.status,
    this.statusLabel,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.isLiked,
    this.user,
  });

  factory ItemRWActivity.fromJson(Map<String, dynamic> json) => ItemRWActivity(
    id: json["id"],
    text: json["text"],
    imagePathFull: json["image_path_full"] == null ? '' : json["image_path_full"],
    likesCount: json["likes_count"],
    commentsCount: json["comments_count"],
    lastUserPostCommentId: json["last_user_post_comment_id"] == null ? null : json["last_user_post_comment_id"],
    lastComment: json["last_comment"] == null ? null : LastComment.fromJson(json["last_comment"]),
    status: json["status"],
    statusLabel: json["status_label"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    createdBy: json["created_by"],
    isLiked: json["is_liked"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "text": text,
    "image_path_full": imagePathFull == '' ? null : imagePathFull,
    "likes_count": likesCount,
    "comments_count": commentsCount,
    "last_user_post_comment_id": lastUserPostCommentId == null ? null : lastUserPostCommentId,
    "last_comment": lastComment == null ? null : lastComment.toJson(),
    "status": status,
    "status_label": statusLabel,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "created_by": createdBy,
    "is_liked": isLiked,
    "user": user.toJson(),
  };

  @override
  List<Object> get props => [id];
}

class LastComment {
  int id;
  int userPostId;
  String text;
  User user;
  int createdAt;
  int updatedAt;
  int createdBy;
  int updatedBy;

  LastComment({
    this.id,
    this.userPostId,
    this.text,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
  });

  factory LastComment.fromJson(Map<String, dynamic> json) => LastComment(
    id: json["id"],
    userPostId: json["user_post_id"],
    text: json["text"],
    user: User.fromJson(json["user"]),
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    createdBy: json["created_by"],
    updatedBy: json["updated_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_post_id": userPostId,
    "text": text,
    "user": user.toJson(),
    "created_at": createdAt,
    "updated_at": updatedAt,
    "created_by": createdBy,
    "updated_by": updatedBy,
  };
}
